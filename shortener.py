#!/usr/bin/python3

import webapp

form = """
    <form action= " " method="POST"> 
        <label>Url: <input type="text" name="url"></label>
        <label>Short: <input type="text" name="short"></label>
        <input type="submit" value="Enviar">
    </form>
"""

class shortener(webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    urls = {}

    def parse(self, request):
        """Return the resource name (including /)"""
        meth = request.split()[0]        # GET resource HTTP/1.1
        res = request.split()[1]
        body = request.split('\r\n\r\n', 1)[-1]

        return meth, res, body

    def process(self, resourceName):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """

        meth, res, body = resourceName

        if meth == "GET":
            if res == "/":
                httpCode = "200 OK"
                htmlBody = "<html><body>Current dictionary: " + str(self.urls) + "<br><br>"+ form + "</body></html>"

            elif res in self.urls.keys():
                httpCode = "301 Moved Permanently"
                htmlBody = "<html><body><meta http-equiv='refresh' content='1;url=" + self.urls[res] + "'></body></html>"

            else:
                httpCode = "404 Not Found"
                htmlBody = "<html><body>" + res + " is not a dictionary content</body></html>"

            return httpCode, htmlBody

        if meth == "POST":

            if body.find("url=") == -1 or body.find("short=") == -1 or body.find("&") == -1:
                httpCode = "400 Bad Request"
                httpBody = "<html><body>Query String format is invalid"

                return httpCode, httpBody

            newbody = body.split("&") #qs --> url=url1&short=short1
            url = newbody[0].split("=")[-1]
            if not url.startswith("http://") and not url.startswith("https://"):
                url = "https://" + url
            short = "/" + newbody[1].split("=")[-1]

            if url != "" and short != "" and short != "/":
                self.urls[short] = url
                httpCode = "200 OK"
                htmlBody = "<html><body>Original URL: " + "<a href='" + self.urls[short] + "'>" + url + "</a>" \
                       + "<br><br>Shorter URL: " + "<a href='" + self.urls[short] + "'>" + short + "</a></body></html>"
            else:
                httpCode = "404 Not Found"
                htmlBody = "<html><body>Wrong urls format</body></html>"

            return httpCode, htmlBody


if __name__ == "__main__":
    testWebApp = shortener("localhost", 1234)